import psutil

def kill_app(name):
    for process in psutil.process_iter():
        if process.name()==name:
            process.kill()

kill_app(input('Kill:    '))
