#!/usr/bin/env python

import sys, os
from PIL import Image

os.chdir('/')

def inp(prompt):
    return raw_input(prompt+':    ')
def inp_int(prompt):
    return int(inp(prompt))

user_inp=inp('Format is BASENAME1...2.png? [BLANK=yes]')
if user_inp=='':
    using_numbers=True
    basename=inp('Base name')
    i=inp_int('First frame number')#0 # initial value
    j=inp_int('Last frame number')
else:
    using_numbers=False


def i_string(i):
    i_str=str(i)
    while len(i_str)<num_digits:
        i_str='0'+i_str
    return i_str

def list_files(i):
    i_str=i_string(i)
    file_name_base=path_main+basename+i_str
    return [file_name_base+'_L.png',file_name_base+'_R.png']

num_digits=4 # e.g. render0045.png
path_main=inp('/Folder/path/')#/media/adam/Burt/blender/2.7/blender/renders/alpha/'
if path_main=='':
    path_main='' # akakaka
elif path_main[-1]!='/':
    path_main=path_main+'/'
path_save=inp('Save path [aa=Same as source]')
if path_save=='aa':
    path_save=path_main
else:
    path_save='/tmp/'


def main(files):
    images = map(Image.open, files)
    widths, heights = zip(*(i.size for i in images))

    total_width = sum(widths)
    max_height = max(heights)

    new_im = Image.new('RGB', (total_width, max_height))

    x_offset = 0
    for im in images:
        new_im.paste(im, (x_offset,0))
        x_offset += im.size[0]
    
    if using_numbers:
        new_im.save(path_save+basename+'_'+i_string(i)+'.png')#'/tmp/'+i_string(i)+'.png')
    else:
        new_im.save(path_save+basename+'.png')

if using_numbers:
    while i<1+j:
        try:
            main(list_files(i))
        except:
            print 'Couldnt stitch '+str(i)+'th file'
            verbose=inp('Verbose? [BLANK=yes]    ')
            if verbose=='':
                main(list_files(i))
        i+=1
else:
    while True:
        basename=inp('Common file base name')
        main([path_main+basename+'_L.png',path_main+basename+'_R.png'])
