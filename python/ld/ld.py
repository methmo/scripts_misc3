import logging, sys, os, time, datetime, random, subprocess
'''
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary  
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.firefox.webdriver import FirefoxProfile
'''

def mute_speakers():
    subprocess.call(['amixer','set','Speaker','mute'])

logger = logging.getLogger('main')
logger.setLevel(logging.DEBUG)

handler = logging.StreamHandler()
handler.setLevel(logging.DEBUG)
logger.addHandler(handler)




path_home=os.path.expanduser('~')
username=path_home.split('/')[-1]

path_ld_dir=os.path.dirname( os.path.abspath(__file__) )

def inp(prompt):
    return input(prompt+':    ')
def inp_int(prompt):
    return int(inp(prompt))
def inp_hour(prompt):
    return 3600*inp_int(prompt+' (hours)')

def mode_current():
    time_current=time_now()
    if time_mode[0]<7: # I.e. we've started this script between midnight and 7am, so we don't have to worry about 24 modulo arithmetic
        mode_scenario='a'
        if time_current<time_mode[1]:
            to_return=0
        elif time_current<time_mode[2]:
            to_return=1
        else:
            to_return=2
    elif time_mode[2]>20:
        mode_scenario='b'
        if time_current<12:
            to_return=2
        elif time_current<time_mode[1]:
            to_return=0
        elif time_current<time_mode[2]:
            to_return=1
        else:
            to_return=2
    elif time_mode[1]>20: # ie 20<time_mode[1]<24, 0<time_mode[2]<12
        mode_scenario='c'
        if time_mode[0]<time_current<time_mode[1]:
            to_return=0
        elif time_current<time_mode[2]:
            to_return=1
        else:
            to_return=2
    else: # ie 20<mode0<24,0<mode1<mode2<12
        mode_scenario='d'
        if (time_current>time_mode[0]) | (time_current<time_mode[1]):
            to_return=0
        elif time_current<time_mode[2]:
            to_return=1
        else:
            to_return=2
    logger.debug('Mode == '+str(to_return)+'        '+mode_scenario)
    return to_return


def playlist_filename(basename):
    if '.m3u' in basename:
        return basename
    else:
        return basename+'.m3u'
def usb_turn(boolean):
    if boolean: # On
        subprocess.call(['sudo', 'ykushcmd', '-d', '3']) # Fans
        subprocess.call(['sudo', 'ykushcmd', '-d', '2']) # Lights
    else:
        subprocess.call(['sudo', 'ykushcmd', '-u', '3']) # Fans
        subprocess.call(['sudo', 'ykushcmd', '-u', '2']) # Lights
def clean_tmp():
    subprocess.call(['rm', '-r', '/tmp/*/webdriver*'])
'''
def remove_trailing_zeros(string):
    try:
        return str(int(string))
    except:
        try:
            return str(string)
        except:
            return string'''
def wikidpad_to_list_cleaned(source_path):
    source=open(source_path).read().split('\n')
    return_list=[]
    for line in source:
        try: # raises exception if line has length < 1
            if (not line[0]+line[1]=='LD')&(not line[0]=='+'):
                return_list.append(line)
        except:
            do_nothing=True
    return return_list


def time_now_raw():
    currentTimes=str(datetime.datetime.now().time()).split(':')
    for i in range(3):
        currentTimes[i]=int(float(currentTimes[i])) # The splits are strings. And '15.1' cannot be int'd, but 15.1 can
    return currentTimes

def time_now():
    currentTimes=time_now_raw()
    currentTime=3600*int(currentTimes[0])+60*int(currentTimes[1])+int(currentTimes[2])
    return currentTime


translate={
    '&':'and',
    '+':'and',
    '@':'at',
    '~':'around',
    '>':'more than',
    '<':'less than',
    '/':'slash',
    '\\':'slash',
    '_':'underscore',
    '-':'dash',
    '*':'star',
    '#':'hash',
}
numbers = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
def say_cleanText(foo):
    foo=string(foo)
    for sym in translate:
        sym_to = ' '+translate[sym]+' '
        foo = foo.replace(sym, translate[sym])
    for n in range(len(numbers)):
        n_to = ' '+numbers[n]+' '
        foo = foo.replace(str(n), no_to)
    return foo.strip()

def say_raw(to_say):
    to_say=str(to_say)
    speak_strings=to_say.split('.')
    for line in speak_strings:
        if line.strip()!='':
            if 'deutsch' in line.lower():
                subprocess.call(['pico2wave', '-l', 'de-DE', '-w', '/tmp/WAV.wav', line])
            else:
                subprocess.call(['pico2wave', '-l', 'en-GB', '-w', '/tmp/WAV.wav', line])
            subprocess.call(['mpv', '--volume', '100', '/tmp/WAV.wav'])
    #print 'Saying directly    '+speak_string

time_last_spoken=0
time_between_speaks=600 # Seconds
def say(to_say):
    to_say=say_cleanText(to_say)
    to_say=string(to_say)
    time_current=time_now()
    if time_current>time_last_spoken+time_between_speaks:
        hour=time_now_raw()[0]
        minute=time_now_raw()[1]
        if time_now_raw()[0]>12:
            am_pm='evening'
            hour=hour-12
        else:
            am_pm='morning'
        time_to_say='Hello {3}. The time is {0} hours {1} minutes in the {2}'.format(hour, minute, am_pm, username)
        say_raw(time_to_say)
        to_return = time_now() # So we can avoid saying the time too frequently
    else:
        to_return = time_last_spoken
    say_raw(to_say)
    return to_return

def mpath(filename):
    #nufn = '{0}/{1}'.format( path_music_playlists, filename )
    nufn = os.path.join(path_music_playlists, filename)
    logger.debug('{0} to {1}'.format(filename, nufn))
    return nufn
def play_fg(filepath):
    logger.debug('Opening '+mpath(filepath))
    logger.debug('Removing \012 from filename')
    fpath = filepath.split('\012')[0] # WTF is this being appended to the string? God damn it.
    #subprocess.call(['see', mpath(fpath)])#, shell=True) # Open command is 'see' on Debian systems
    subprocess.call(['mpv', mpath(fpath)]) # Why in god's name does it open fucking VLC as default here when it doesn't outside Python? VLC not terminating or sleeping a bit on errors killing my CPU
def play(filepath):
    play_fg(filepath)
def play_bg(filepath):
    logger.debug('POpening '+mpath(filepath))
    subprocess.Popen(['see', mpath(filepath)])#, shell=True)

media_fg_current=''
def media_fg_new():
    playlist_current = random.choice(playlists_list_current)
    media=media_fg_current
    while media==media_fg_current:
        media=random.choice(playlists_list_current)
    return media
media_bg_current=''
def media_bg_new():
    playlist_current = random.choice(playlists_bg[mode])
    media=media_bg_current
    while media==media_bg_current:
        media=random.choice(playlists_bg[mode])
    return media

def populate_playlist(fg_bg):
    logger.debug('Populating playlist '+fg_bg)
    list_fg_bg=[]
    for i in range(modes_num): # One list of playlists for each mode. range(3)=0,1,2
        logger.debug('Populating playlist '+fg_bg+' '+str(i))
        filepath = os.path.join(path_ld_dir, '~playlists_{0}_{1}.txt'.format(fg_bg, str(i)))
        list_new=[]
        try:#if os.path.isfile( filepath ):
            with open( filepath, 'r') as f:
                logger.debug('Opened file '+filepath)
                for line in f:
                    if line.strip()!='':
                        pname = playlist_filename(line)
                        ppath = os.path.join(path_music_playlists, pname )
                        try:#if os.path.isfile( ppath ): # This fails for some reason
                            list_new.append(pname)
                            logger.debug('Added playlist '+pname)
                        except:
                            logger.debug('Playlist '+ppath+' does not exist')
                f.close()
        except:
            logger.debug('Could not find file '+filepath)
        list_fg_bg.append(list_new)
    logger.debug('Finished populating playlist '+fg_bg)
    logger.debug(list_fg_bg)
    return list_fg_bg

def read(string):
    lines=string.split('\n')
    for line in lines:
        to_play={}
        say_raw(string)
        for effect in sound_effects:
            logger.debug('Looking for special effect '+effect)
            if ' '+effect in line.lower():
                # We want to see where the effect is in the line, hence we want to compare sizes of the strings preceding or after it.
                line = '@'+line+'@' # Avoids special cases
                pos=line.split(effect)
                order = len(pos[0])
                to_play[random.choice(sound_effects[effect])]=order
                logger.debug('Special effect found  '+effect)
        for i in range(9999):
            for effect in to_play:
                if to_play[effect]==i: # Order of the effect
                    play(effect)


def main():
    media_fg_current=media_fg_new()
    logger.debug('Playing '+media_fg_current)
    play_fg(media_fg_current)

'''
profile = FirefoxProfile('/home/adam/.mozilla/firefox/i0hoawps.default/')
driver = webdriver.Firefox(profile)
def yt_random(search_tags):
    search_url='https://www.youtube.com/results?search_query='+search_tags.replace(' ','+')+'&sp=EgIYAVAU'
    driver.get(search_url)
    actions = ActionChains(driver) 
    actions.send_keys(Keys.TAB * 6)
    actions.perform()
    for i in range(random.randint(1,10)):
        actions = ActionChains(driver) 
        actions.send_keys(Keys.TAB * 5)
        actions.perform()
    actions = ActionChains(driver) 
    actions.send_keys(Keys.ENTER)
    actions.perform()
def yt_seffect(tags):
    yt_random('sound effect '+tags)
'''

mute_speakers()


time_mode=[]
modes_num=3
time_mode.append( time_now() )
time_mode.append( inp_hour('Time assume asleep') )
time_mode.append( inp_hour('Time assume dreaming') )

path_music=os.path.normpath( inp('Music path') )# Normalises /media/foo/Music/ to /media/foo/Music os.path.join(user_path,'Music')
if path_music.strip()=='':
    with open( '/tmp/path_music', 'r') as f:
        for line in f:
            if line.strip()!='':
                path_music=line
                logger.debug('Var path_music = '+path_music)
        f.close()
else:
    subprocess.call(['touch','/tmp/path_music'])
    with open( '/tmp/path_music', 'w') as f:
        f.write(path_music)
        f.close()
path_music_playlists=os.path.join( path_music, 'playlists')





sound_effects={}
path_sound_effects=os.path.join( os.path.dirname(path_music), 'sound-effects')
path_sound_effects_list=os.path.join( path_sound_effects, 'list.csv')
with open( path_sound_effects_list, 'r') as f:
    logger.debug('Opened file '+path_sound_effects_list)
    for line in f:
        l = line.strip()
        if l!='':
            filename=l.split(',')[0]
            filepath=os.path.join(path_sound_effects, filename)
            tags=l.partition(',')[2].lower().split(',')
            for tag in tags:
                if tag.strip()!='':
                    try:
                        sound_effects[tag].append(filepath)
                    except:
                        sound_effects[tag]=[filepath]
    f.close()
logger.debug(sound_effects)






playlists_fg=populate_playlist('fg')
playlists_bg=populate_playlist('bg')
logger.debug(playlists_fg)
logger.debug(playlists_bg)

usb_turn(0)
mode=0

read(inp('Read (test)'))

while mode_current()==0:
    playlists_list_current=playlists_fg[0]
    main()
while mode_current()==1:
    playlists_list_current=playlists_fg[1]
    main()
