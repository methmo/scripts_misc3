import os, subprocess

user_path = os.path.expanduser('~')
path = os.path.join( user_path, 'bin', 'git', 'config', 'firefox', '57', 'addon_list.txt' )

with open(path ,'r') as f:
    for line in f:
        subprocess.call(['firefox','https://addons.mozilla.org/en-US/firefox/addon/{0}/'.format(line)])
    f.close()
