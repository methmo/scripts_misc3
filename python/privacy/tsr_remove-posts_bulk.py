import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def inp(prompt):
    return input(prompt+':    ')

username=inp('Username')
password=inp('Password')

user_id=inp('User ID')

signin_url='https://www.thestudentroom.co.uk/signin.php'
posts_url='https://www.thestudentroom.co.uk/search.php?u={0}&userid={0}&tracksoft=&crr=&filter[userid]={0}&page='.format(user_id)

overwrite=inp('Error message')


#Login
driver = webdriver.Firefox()
driver.get(signin_url)
driver.set_window_size(100, 100) # So it aint obnoxiously large and bright

usernameForm = driver.find_element_by_name('vb_login_username')
passwordForm = driver.find_element_by_name('vb_login_password')
usernameForm.send_keys(username)
passwordForm.send_keys(password)

driver.find_element_by_xpath(".//button[contains(.,'ign in')]").click()
#/Login


wait = WebDriverWait(driver, 10)






i=101 # Start at 100th page of the post history
j=0 # Start at first post on that page
while i<283: # Total number of pages, roughly
    while j<25: # Theere are 25 posts per page on the post history
        # Post history page
        driver.get(posts_url+str(i))
        #time.sleep(3)

        # Go to last page of history
        #link = driver.find_element_by_xpath("//a[contains(@title, 'Last Page')]")
        #link.click()

        # Click on the first post link
        #links = driver.find_elements_by_xpath("//a[contains(@href, '&highlight=')]")
        link = driver.find_elements_by_xpath("//a[contains(@href, '&highlight=')]")[j]
        link.click()

        # Click on 'edit' button
        while True:
            try:
                link = driver.find_element_by_xpath("//a[contains(@href, 'editpost.php?p=')]")
                link.click()
            except:
                print('Locating element editpost.php...')
        '''Now, there are two possibilities:
        Either we are sent to a new webpage for editing the post, or we stay on the same page.

        The text areas are the same in both cases.'''

        #try:
        # Edit text to nonsense
        #print(driver.page_source) # debugging, since page source for selenium seems a bit different to that of my own browser
        time.sleep(10) # Maybe will stop having some issues with not finding the following elements
        text_area = driver.find_element_by_class_name('input-textarea')#post-content postcontent restore ui-autocomplete-input')
        # Alert user and ask for permission
        #permission=inp('Overwrite this? [blank=yes],[non-blank=no]')
        #if permission=='': # Seems to give error...
        # Edit text to the overwrite
        try:
            text_area.clear()
        except:
            print('Failed to clear text_area to:'+overwrite)
            print(posts_url+str(i)+'    j '+str(j))
            print('Probably error: Element is not currently interactable and may not be manipulated')
        text_area.send_keys(overwrite)
        #Submit changes
        try:
            try:
                driver.find_element_by_xpath(".//button[contains(.,'Save Changes')]").click()
            except:
                driver.find_element_by_xpath(".//button[contains(.,'save')]").click()
        except:
            print('Failed to save changes')
            print(posts_url+str(i)+'    j '+str(j))
        j+=1
    i+=1

# Click on delete button
#link = driver.find_element_by_xpath("//button[contains(., 'elete')]")
