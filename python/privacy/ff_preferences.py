import json, os#, subprocess

path_parent = os.path.join( os.path.expanduser('~'), '.mozilla', 'firefox' )
path = os.path.join( path_parent, '*.default')
path_prefs = os.path.join( path, 'prefs.js' )

with open(path_prefs, 'r', encoding='utf-8') as f:
    data = json.load(f)
    data['general.platform.override'] = 'Win32' # Platform corresponding to Tor
    data['general.appversion.override'] = '5.0 (Windows NT 6.1; rv:52.0) Gecko/20100101 Firefox/52.0'
    data['general.buildID.override'] = ''
    data['general.oscpu.override'] = ''
    data['general.useragent.override'] = 'Mozilla/5.0 (Windows NT 6.1; rv:52.0) Gecko/20100101 Firefox/52.0' # This is the user agent used by Tor, therefore is very common
    data['general.useragent.vendor'] = 'Google Inc.'
    data['browser.search.countryCode'] = 'US'
    data['browser.search.region'] = 'US'
    data['extensions.agentSpoof.excludeList'] = "W3,W1,W5,W6,W9,W23,W22,W21,W20,W19,W17,W15,W14,W13,W12,W11,W7,W4,W27,W49,W48,W47,W43,W42,W41,W40,W38,W37,W36,W35,W40,W45,W28,W29,W30,W31,W32,W34,W51,W53,W56,W68,W67,W69,W54,W75,W74,W73,W72,W70,M25,M24,M26,M27,M32,M33,M34,M35,M40,L7,L9,L11,L13,L14,L15,L16,L17,L18,L19,L21,L20,L22,L23,L23,L25,L26,L30,L31,L32,L33,L34,L35,U14,U13,U12,U11,U10,U9,U8,U7,U6,U5,U4,U3,U2,U1,U15,A5,A6,A7,A10,A11,A12,A13,A14,A15,F8,F10,F11,F9,F7,F6,F5,F4,F3,F2,F1,F12"
    data["browser.cache.disk.capacity"] = "358400"
    data["browser.cache.disk.enable"] = "false"
    data["browser.cache.disk.filesystem_reported"] = "1"
    data["browser.cache.disk.hashstats_reported"] = "1"
    data["browser.cache.disk.smart_size.first_run"] = "false"
    data["browser.cache.disk.smart_size.use_old_max"] = "false"
    data["browser.cache.frecency_experiment"] = "1"
    data["browser.cache.memory.capacity"] = "-1" # Unlimited
    data["browser.download.dir"] = "/tmp"

os.remove(path_prefs)
with open(path_prefs, 'w', encoding='utf-8') as f:
    json.dump(data, f, indent=4)

#subprocess.call(['firefox','https://leotindall.com/tutorial/locking-down-firefox/'])
