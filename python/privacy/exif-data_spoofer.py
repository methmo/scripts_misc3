#!/usr/bin/env python3

# Written for Linux (*buntu), requires package exiftools

import random, time, subprocess

file = input('File path:    ')

#
# Credit to Tom Alsberg from StackOverflow, 2009
def unform_time_between(a, b, format):
    time_a = time.mktime(time.strptime(a, format))
    time_b = time.mktime(time.strptime(b, format))
    t = random.uniform(0,1)
    time_output = stime + t * (etime - stime)
    return time.strftime(format, time.localtime(time_output))

def random_modificationDate():
    return strTimeProp('2010:01:01 00:00:00-00:00', '2017:10:01 23:59:59-59:00', '%Y/%m/%d %H:%M:%S-')

def random_date():
    return strTimeProp('2010:01:01 00:00:00', '2017:10:01 23:59:59', '%Y/%m/%d %H:%M:%S')

def add_to_date(a, b):
    return strTimeProp(a, '2017:10:01 23:59:59', '%Y/%m/%d %H:%M:%S')
# End Credit
#

print randomDate("1/1/2008 1:30 PM", "1/1/2009 4:50 AM", random.random())

d = {}

d['DateTimeOriginal'] = random_date() # Date-time of the shutter closing
d['CreateDate'] = DateTimeOriginal[:1] + str((int(DateTimeOriginal[-1]) + 1)%60) # Date-time of the file being created
GPS_tmp = DateTimeOriginal.split(' ')
d['GPSDateStamp'] = GPS_tmp[0]
d['GPSTimeStamp'] = GPS_tmp[1]

for entry in d:
    name = entry
    value = d[entry]
    subprocess.call(['exiftool','-{0}={1}'.format(entry,d[entry]),file])
