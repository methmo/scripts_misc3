#!/usr/bin/env python3

import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


driver = webdriver.Firefox()
driver.get('http://www.wordgenerator.net/fake-word-generator.php')

i=0
while i<800:
    driver.find_element_by_id('rnames').click()
    time.sleep(0.1)
    i+=1
