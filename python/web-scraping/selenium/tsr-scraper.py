#!/usr/bin/env python3

# Very old (and crap) python3 script to scrape TSR threads and print them to a simplified file

import requests, time, random
from bs4 import BeautifulSoup

def printIf(string,condition):
    if condition:
        print(string)
lineIndent='    '
dlineIndent='        '

specialCharList=['%','$','£','"','\'','!','^','&','*',':',';','@','#','~','<',',','.','>','/','?','\\','|']

headers={'User-Agent':'Mozilla/5.0'} # if not, the site will ban your, because the default User-Agent is crawler, which the site do not like.
pageNumber=1
postcount=0

def inp(prompt):
    return input(prompt+':    ')

while True:

    targetNumber=input('Thread number:    ')
    targetUrl='http://www.thestudentroom.co.uk/showthread.php?t='+str(targetNumber)+'&page='+str(pageNumber)
    #targetUrl=input('Url:    ')


    targetSource=requests.get(targetUrl)
    targetSoup=BeautifulSoup(targetSource.text, 'lxml')
    #targetSoup=BeautifulSoup(targetSource, 'html.parser')
    targetTitle=targetSoup.title.text # Works!

    #targetPosts=targetSoup.find('ol', attrs={'class': 'posts'})
    #print(targetPosts.li)

    targetPosts=targetSoup.find('div', attrs={'class':'post-content'})
    #print(str(targetPosts.blockquote.text.strip()))



    #usernames=[]
    #usernameData = targetSoup.find_all('div',attrs={'class':'username expandable'})
    #for username in usernameData:
    #    username=BeautifulSoup(username,'lxml')
    #    usernames.append(username.a.text.strip())
    #    print(username)


    #try:
    ids=[]
    idData = targetSoup.find_all('a',attrs={'class':'postanchor'})
    for ID in idData:
        nummm=len('<a class="postanchor" id="post')
        mummm=len('"></a>')
        ids.append(str(ID)[nummm:][:mummm])
        
    posts=[]
    postData = targetSoup.find_all('blockquote',attrs={'class':'postcontent restore'})
    #imgData = targetSoup.find('blockquote',attrs={'class':'postcontent restore'}).img['src']
    for post in postData:
        
        #images.append([])
        #currentimages=BeautifulSoup(str(post),'lxml').select('img')
        #currentimageNames=[]
        #for currentimage in currentimages:
        #    #currentimagesDeeper=BeautifulSoup(str(currentimage),'lxml').select('title')
        #    print(currentimage)
        #    testt=BeautifulSoup(str(currentimage),'lxml')
        #    testt.find('div', {"class": "owner"}).img['src']
        #    
        #    #print(testt)
        #    #testty=testt.find(itemprop='image')
        #    #print(testty)
        #    #print(BeautifulSoup(str(currentimage),'lxml').src)
        #    #print(BeautifulSoup(str(currentimage),'lxml').img.src)
        #
        #    #print(currentimagesDeeper)
        #    #images[postcount].append(currentimage)
        
        posttext=BeautifulSoup(str(post),'lxml').text.strip()
        
        
        postLines=posttext.split('\n')
        postLinesClean=[]
        nextlineisquote=False
        for line in postLines:
            if '   ' not in line: # Tests for quotes, which appear as '       ' before the quoted text
                nextlineisquote=False
            if nextlineisquote & (line.strip()!=''): # The latter prevents empty lines being assigned characters and becoming nonempty
                line=nextlineadd+lineIndent+line.strip()
            elif 'Original post by ' in line:
                line=line.replace('Original post by ','QUOTE:    ')
                nextlineisquote=True
                nextlineadd=line
                line='' # This makes the 'QUOTE:USERNAME' appear on the next line, with the first line of quoted text
            elif 'Spoiler' in line:
                line='\n'+line
            elif 'Posted from ' in line:
                line=''
            if line.strip()!='':
                postLinesClean.append(line)
        posttext=''
        for line in postLinesClean:
            posttext=posttext+lineIndent+line+'\n'
        posts.append(posttext)
        
        postcount+=1
        #posts.append(BeautifulSoup(str(post),'lxml').text.strip())
        
    #for post in posts:
    #    print(post)

    users=[] # Note that this is a list, not a set of users. So same user might occur multiple times, in order of his posts.
    userData = targetSoup.find_all('div', attrs={'class':'username expandable'} )
    for user in userData:
        user=user.text.strip()
        user=user.partition('\n')[0]
        users.append(user)
        
    reps=[] # Note that this is a list, not a set of reps. So same rep might occur multiple times, in order of his posts.
    repData = targetSoup.find_all('div', attrs={'class':'post-reputation'} )
    for rep in repData:
        rep=rep.text.strip()
        rep=rep.partition('\n')[0]
        reps.append(rep)
        
    dates=[] # Note that this is a list, not a set of dates. So same date might occur multiple times, in order of his posts.
    dateData = targetSoup.find_all('span', attrs={'class':'datestamp'} )
    for date in dateData:
        date=date.text.strip()
        date=date.partition('\n')[0]
        dates.append(date)
        
        

    for i in range(len(users)):
        j=i
        print(str(postcount)+lineIndent+users[j]+lineIndent+reps[j]+lineIndent+dates[j]+lineIndent+ids[j])
        print(posts[j])
        #print(images[j])
        print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
    
    print('Archived page '+str(pageNumber))
    pageNumber+=1
    randy=random.uniform(5, 15)
    while randy>1:
        time.sleep(1)
        print('Waiting 1 second')
        randy-=1
    print('Waiting '+randy+' seconds')
    time.sleep(randy)
