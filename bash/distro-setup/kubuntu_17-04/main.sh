#!/bin/bash

source functions.sh || source ~/bin/git/scripts_misc/bash/distro-setup/kubuntu/functions.sh # Redundancy in case user has not called this script from its parent folder


# ==========================
# START CUSTOMISABLE SECTION
# ==========================
# --------------------------------------
# List of installation functions to call
# --------------------------------------
# NB: Since the script was originally designed to be non-modular, there might be some dependency issues since transition to modular.
# Some functions have two parts - one that is headless, and one that requires user interaction. The latter is bundled into a seperate function, that is to be run at the end, if the trigger variable is set to TRUE
# ========
# WARNING!
# ========
#   Variables not defined beforehand will wrongly make "if $var; then" trigger as TRUE.
#   Hence we ask "if [ "$var" = true ]; then"
# Comment in/out to install/skip a category
install_category_core    
install_category_common
#
install_category_dev
install_category_neuralnets
install_megacategory_python
#
install_category_nice
install_category_privacy
install_category_network
#
#virtual_machines
#
#gaming
#gaming_emulation
#
#video_conferencing
#media_editing
3d_modelling
#3d_modelling_gamedev
overclocking_tweaks # Fan curves # User input
# Quality of life
add_functions_to_usr_bin
# Issues with booting etc
grub_menu_nonzero_hidden_timeout
# CryptoCurrencies
mine_musicoin
# ========================
# END CUSTOMISABLE SECTION
# ========================





# =====================================
# Scripts that require user interaction
# =====================================
# Python IDE (part that requires user interaction)
# umake ide pycharm # To uninstall: umake -r ide pycharm
if [ "$var_nvidia_driver" = true ]; then # WARNING IMPORTANT! Nouveou driver clashes horribly with nvidia drivers, must disable else get weird errors at boot. Sometimes the errors are not obviously caused by the driver, so might cause damage by wrong solution.
    cd /etc/default
    sudo cp grub grub_backup
    sudo sed -i 's/"quiet splash/"nomodeset nouveau.blacklist=1 quiet splash/g' grub # Replace
    sudo update-grub2
fi
# If boot/driver problems persist, first try to boot into recovery mode, then resume to normal mode. Then edit the file above by hand. (There is problem accessing grub from recovery root)
#   If you need to use a Live USB to edit grub:
#   Boot into Live USB
#   sudo mount /dev/sdX9 {ie root} /media/kubuntu/{DISK ID}/boot
#   sudo chroot /media/kubuntu/{DISK ID}
#   sudo update-grub2

cd $scriptPath
bash configureKWin.sh

if [ "$var_dns_to_custom_ethernet" = true ]; then
    dns_to_custom_ethernet
fi
if [ "$var_dns_to_custom_wifi" = true ]; then
    dns_to_custom_wifi
fi
if [ "$var_ff" = true ]; then
    bash $anyBuntuPath/ff_setup_gui.sh
fi
if [ "$var_git_ssh_keygen" = true ]; then
    xclip -sel clip < ~/.ssh/id_rsa.pub # Copy contents to clipboard
    printf '\nKey copied to clipboard.'
    firefox https://github.com/settings/keys https://gitlab.com/settings/keys
fi
if [ "$var_gaming_emulation" = true ]; then
    gaming_emulation_gui
fi
if [ "$var_3d_modelling_gamedev" = true ]; then
    3d_modelling_gamedev_gui
fi
if [ "$var_javadev" = true ]; then
    javadev_gui
    cd $scriptPath
    sudo bash ~set-wallpaper.sh
fi
if [ "$var_privacy" = true ]; then
    install_with_feedback macchanger
fi
if [ "$var_nvidia_cuda" = true ]; then # Near the end because it might fail.
    install_with_feedback gcc-5 # Older version of gcc
    cd /usr/bin
    sudo unlink gcc
    sudo ln -s gcc-5 gcc
    cd /tmp
    d=$(date +%Y-%m-%d)
    touch /tmp/file-list
    cudaDownload_found=false
    cudaDownload_downloaded=false
    until [ "$cudaDownload_found" = true ]; do # Tests whether grep returns exit code 0 or 1, i.e. whether it has found "cuda" in the list or not
        e=$(echo $d | tr -d -)
        wget https://developer.nvidia.com/compute/machine-learning/cudnn/secure/v$cudnnVersion/prod/${cudaVersion}_$e/cudnn-${cudaVersion}-linux-x64-v${cudnnVersion%%.*} >> /tmp/cuda_wget_attempts
        wget_output=$(wget -q "$url") && cudaDownload_found=true && cudaDownload_downloaded=true
        touch /tmp/file-list # Reset the list
        ls /tmp >> /tmp/file-list
        if [ grep -F "cuda" /tmp/file-list > /dev/null ]; then
            cudaDownload_found=true
            cudaDownload_downloaded=true
        elif [ grep -F "ERROR 403: Forbidden" /tmp/cuda_wget_attempts > /dev/null ]; then
            cudaDownload_found=true
            cudaDownload_downloaded=false
        else
            d=$(date -I -d "$d - 1 day") # Deduct a day from d
        fi
    done
    if [ "$cudaDownload_downloaded" = false ]; then
        read -p "Login to NVidia and download the file manually" dummyVar
        firefox https://developer.nvidia.com/compute/machine-learning/cudnn/secure/v$cudnnVersion/prod/${cudaVersion}_$e/cudnn-${cudaVersion}-linux-x64-v${cudnnVersion%%.*}
    fi
    tar -xzvf cudnn-*.tgz # Untars to directory "cuda"
    sudo cp cuda/include/cudnn.h /usr/local/cuda/include
    sudo cp cuda/lib64/libcudnn* /usr/local/cuda/lib64
    sudo chmod a+r /usr/local/cuda/include/cudnn.h
    
    # Find latest gcc version
    apt-cache search gcc | egrep "gcc-[0-9] " >> /tmp/gcc-versions # output is lines e.g. "gcc-7 GNU C compiler"
    gccVersionText=$(tail -1 /tmp/abcde) # would be eg. "gcc-7 GNU C compiler"
    gccVersion=${gccVersionText%% *} # Extract text before whitespace
    sudo unlink gcc
    sudo ln -s $gccVersion gcc
fi
if [ "$var_musicoin" = true ]; then
    printf "To set up MusicCoin, you must:\n    gmc console\n    admin.addPeer(\"enode://abcde@1.2.3.4:30303\") for a couple of enodes (more if trouble connecting)"
    read -p "Press ENTER to continue" dummyVar
    firefox https://github.com/Musicoin/go-musicoin/wiki/Boostrap-enodes
    printf "\n\nNow, we must create a new coinbase account:\n    personal.newAccount()"
    read -p "Press ENTER to continue" dummyVar
    printf "\n\nExit the console and go to backup this wallet at ~/.ethereum/keystores\n  Save the key file to other places to make sure you can access the balance from other wallets"
    read -p "Press ENTER to continue" dummyVar
fi
if [ "$var_virtual_machines" = true ]; then # Must be last, since log out
    virtual_machines_gui
fi
