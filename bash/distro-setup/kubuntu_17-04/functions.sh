#!/bin/bash

# NB: Quick customisation options are around page 760, marked by the string $$$$

echo 'Kubuntu_17-04.sh version 0.94' # For use with self-updating feature using rsync

#cd "${0%}" #This CDs to this script's directory. Works for whitespaces in path (hence the ""), fails if this script is called from another"

repoName=scripts_misc

gitPath=~/bin/git # NB: this is expanded NOW, not later - i.e. for ssh, don't use this for other systems
mainPath=$gitPath/$repoName # ~ wouldn't work with it as a literal string
distroSetupPath=$mainPath/bash/distro-setup
scriptPath=$distroSetupPath/kubuntu
pythonPath=$mainPath/python
anyBuntuPath=$distroSetupPath/+buntu
bashPath=$mainPath/bash
bashrc=$distroSetupPath/bashrc

sudo mount -t tmpfs -o size=4G tmpfs /tmp

add_repository () {
    args=("$@")
    for ((i=0;i<$#;i++)); do
        sudo add-apt-repository -y "${args[$i]}" || sudo add-apt-repository -y "${args[$i]}" # E.g. FF nightly repository failed first time with 'failed to add key'; second time it was fine.
    done
}

read -p 'Update script, or copy restricted files over LAN? [y/n]:    ' copyTheRestricted # Since copyRestricted variable name is ambiguous
if [ "$copyTheRestricted" = "y" ]; then
    printf '\n\n\nHint: to see fingerprint on server, run:\n    ssh-keygen -l -f /etc/ssh/ssh_host_ecdsa_key.pub\n'
    read -p 'Remote user:    ' remoteUser
    read -p 'Remote IP:    ' remoteIP
    rsync -chazP --stats $remoteUser@$remoteIP:/home/$remoteUser/bin/git/$repoName $gitPath # NB: sending to $mainPath would create a folder called scripts_misc inside scripts_misc
    bash $scriptPath/kubuntu_17-04.sh # Run the new script
    exit 0
elif [ "$copyTheRestricted" = "n" ]; then
    echo 'Okay'
    # No exit, since we want the new script to run fully (we answer 'n' to the new script)
else
    echo 'Please enter either "y" or "n"'
    exit 0
fi
read -p '(Optional) GitLab email:    ' gitmail # for github and gitlab
if [ "$gitmail" = "" ]; then
    var_git_dev=false
else
    var_git_dev=true
    read -p 'GitLab username:    ' gitname
fi
read -p '(Optional) CUDA version (eg 8.0 or 9.1):    ' cudaVersion
read -p '(Optional) IP address of DNS:    ' dns_ip
home=$HOME
add_repository ppa:graphics-drivers/ppa # Placed before others to minimise user wait time for input
#apt-cache search nvidia- | grep driver | grep -v dev
#read -p 'Latest NVidia driver:    nvidia-' latestNVidiaDriver

sudo -v #Prompts for SU password. Later on, it refreshes the script's SU credentials."
kcmshell5 screenlocker --caption "Set screen lock time to 30 minutes or more"

mv "$HOME/bin/bash/_newInstall_errors_old" "$HOME/bin/bash/_newInstall_errors_older"
mv "$HOME/bin/bash/_newInstall_errors" "$HOME/bin/bash/_newInstall_errors_old"
mv "/tmp/_newInstall_errors" "$HOME/bin/bash/_newInstall_errors"



printout (){
    printf "\n\n\n$1"
}
keep_hdds_running () { # Keep em spinning, to stave off hard drive death. Script run at startup
    IFS=', ' read -r -a args <<< "$hdd_str"
    sudo touch /etc/init.d/keep-hdds-running.sh
    echo "#!/bin/bash" | sudo tee --append /etc/init.d/keep-hdds-running.sh
    echo "while true; do" | sudo tee --append /etc/init.d/keep-hdds-running.sh
    for ((i=0;i<${#args[@]};i++)); do
        "    touch media/$name/${args[$i]}" | sudo tee --append /etc/init.d/keep-hdds-running.sh
    done
    echo "    sleep 60" | sudo tee --append /etc/init.d/keep-hdds-running.sh
    echo "done" | sudo tee --append /etc/init.d/keep-hdds-running.sh
    sudo chmod +x /etc/init.d/keep-hdds-running.sh
}
install_with_feedback () {
    args=("$@")
    for ((i=0;i<$#;i++)); do # $# is number of arguments passed to the function. #@ does 'for all arguments'
        echo "Installing ${args[$i]}";
        sudo apt -y install ${args[$i]} || echo "Installation of ${args[$i]} failed" >> "~/tmp/_newInstall_errors";
    done;
}
pip_install () {
    args=("$@")
    for ((i=0;i<$#;i++)); do # $# is number of arguments passed to the function. #@ does 'for all arguments'
        echo "Pip installing ${args[$i]}";
        pip install ${args[$i]} || sudo -H pip install ${args[$i]};
    done;
}
pip3_install () {
    args=("$@")
    for ((i=0;i<$#;i++)); do # $# is number of arguments passed to the function. #@ does 'for all arguments'
        echo "Pip3 installing ${args[$i]}";
        pip3 install ${args[$i]} || sudo -H pip3 install ${args[$i]};
    done;
}


# Define installation functions (to allow quick disabling/enabling)
# WARNING: empty functions will cause error
env_var () {
printf 'Bash path'
    # echo 'fi' >> ~/.profile # This method isn't permanent, so we instead modify ~/.profile. Note that ~/bash_profile overrides this, if it exists, ~/.bash_profile
    echo "if [ -d "$HOME/bin/bash" ] ; then" >> ~/.profile
    echo '    PATH="$HOME/bin/bash:$PATH"' >> ~/.profile
    echo 'fi' >> ~/.profile
}
apt_accio () { # Credit: u/DuBistKomisch of reddit.com
    echo "alias accio='apt install'" | sudo tee --append ~/.bashrc
    echo "alias sudo='sudo ' # Otherwise sudo parses aliases as their own arguments, rather than as an alias" | sudo tee --append ~/.bashrc
}
ramdisk_permanent () {
    cp /etc/fstab /etc/fstab_backup
    echo tmpfs /tmp tmpfs nodev,nosuid,noexec,nodiratime,size=4G 0 0 | sudo tee --append /etc/fstab # ensures it is mounted every boot
    echo tmpfs /tmp tmpfs mode=1777,nosuid,nodev,size=4G 0 0 | sudo tee --append /etc/fstab
    var_ramdisk_permanent=true
}
hdds_keep_running () {
    read -p 'HDD1, HDD2, ...:     ' hdd_str #so can write a script to keep them running at all times (to reduce risk of DEATH)
    sudo hdparm -S 250 /dev/sdb && sudo hdparm -S 250 /dev/sdc && sudo hdparm -S /dev/sdd && sudo hdparm -S /dev/sde
    keep_hdds_running
}
video_conferencing () {
    install_with_feedback cheese # WebCam
    cd /tmp
    wget https://go.skype.com/skypeforlinux-64.deb
    sudo gdebi skypeforlinux-64.deb
    rm ~/.config/autostart/skype*
}
nice_to_haves () {
    install_with_feedback xclip # Allows command-line copying to clipboard
    install_with_feedback tree
    if [ "$var_python3_core" = true ]; then
        python3_thefuck # Handy tool that corrects last command
    fi
}
vlc_pause_without_headphones () {
    printout 'Ensuring VLC pauses when headphones are taken out'
    # Get the files from https://askubuntu.com/questions/521371/how-to-pause-vlc-playback-when-the-headphones-are-disconnected
    # Doesn't seem to work :(
    cd $distroSetupPath
    sudo cp headphone_jack.sh /etc/acpi/headphone_jack.sh
    sudo chmod +x /etc/acpi/headphone_jack.sh
    sudo cp headphones /etc/acpi/events/headphones
    sudo service acpid restart # So this functionality is immediately available
}
media_players () {
    install_with_feedback vlc
    #vlc_pause_without_headphones
}
commandline_utils_bloat () {
    install_with_feedback terminology
    install_with_feedback taskwarrior qalc remind # productivity,calculator,calendar
    install_with_feedback moc # Music on command line
    install_with_feedback rtv # redditViewer
    install_with_feedback lynx # CL browser
    install_with_feedback w3m w3m-img # command line browser
    install_with_feedback cmatrix cmatrix-xfont libaa-bin caca-utils bb nmap # For a Hollywood 1337 h4ck3r setup
}
git_core () {
    install_with_feedback git gitk
    git config --global alias.lgb "log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset%n' --abbrev-commit --date=relative --branches"
    # Credit:https://stackoverflow.com/questions/2421011/output-of-git-branch-in-tree-like-fashion
}
git_ssh_keygen () {
    install_with_feedback xclip # Requirement. Allows command-line copying to clipboard
    cd ~
    printf 'Leave filename blank to use default' # Because I'm an idiot
    ssh-keygen -t rsa -b 4096 -C "$gitmail"
    eval "$(ssh-agent -s)" # Starts SSH agent in background
    ssh-add ~/.ssh/id_rsa
    var_git_ssh_keygen=true
}
git_dev () {
    if [ "$var_git_dev" = true ]; then
        git config --global user.email "$gitmail"
        git config --global user.name "$gitname"
        git_ssh_keygen
    fi
}
git_nice () {
    if [ "$var_python3_core" = true ]; then
        pip3_install gitsome
    fi
}
installation_tools () {
    install_with_feedback gdebi gdebi-core #GDebi: Installs .deb files easily, respecting dependencies
    install_with_feedback apt-file # Command line tool to search within packages
    install_with_feedback build-essential cmake extra-cmake-modules yasm # Basic compilers
    git_core
}
webget_core () { # Things depend on this
    install_with_feedback curl
}
webget () {
    install_with_feedback youtube-dl
    #install_with_feedback uget # Video downloader
}
webget_adv_badown () {
    mkdir -p ~/bin/tools/web-scraping/media
    cd ~/bin/tools/web-scraping/media
    git clone https://github.com/stck-lzm/badown/ # Easier command-line downloading from mega.nz, mediafire
    cd badown
    touch multiple.sh
    echo "read -p 'Urls:    ' urls" >> multiple.sh
    echo "for i in \$(cat urls); do ./badown \$i && sleep 1.5; done" >> multiple.sh
}
webget_adv () {
    webget_adv_badown
}
browser_chrome () {
    cd /tmp
    wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
    sudo gdebi google-chrome-stable_current_amd64.deb
    #sudo dpkg -i google-chrome-stable_current_amd64.deb
    rm google-chrome-stable_current_amd64.deb
}
browsers_ff () {
    # Cache to RAMDisk. Credit: https://support.mozilla.org/en-US/questions/1066350 http://kb.mozillazine.org/User.js_file
    if [ "$var_ramdisk_permanent" = true ]; then
        echo '// Relocate parent directory for browser cache' >> ~/.mozilla/user.js
        #sudo mkdir /tmp/firefox
        echo 'user_pref("browser.cache.disk.parent_directory", "/tmp");' >> ~/.mozilla/user.js
    fi
    TZ=UTC firefox # Sets TimeZone to UTC to reduce fingerprinting
    cd /tmp
    git clone -b relaxed https://github.com/pyllyukko/user.js
    mv user.js/user.js ~/.mozilla/firefox/*.default
    python3 $pythonPath/privacy/ff_preferences.py
    var_ff=true
}
browsers_qute () {
    #printf 'VIM-ish browser' # ~13 MiB?
    cd /tmp
    wget https://qutebrowser.org/python3-pypeg2_2.15.2-1_all.deb
    sudo apt -y install ./python3-pypeg2_2.15.2-1_all.deb
    rm python3-pypeg2_2.15.2-1_all.deb
    # We're going to use github's API to get the latest version. And some very crude regex.
    curl -s https://api.github.com/repos/qutebrowser/qutebrowser/releases/latest | egrep -o 'http.*\.deb' | wget -i - -O qutebrowser.deb
    sudo apt -y install ./qutebrowser.deb
    rm qutebrowser.deb
}
browsers () {
    #browser_chrome
    browsers_ff
    #browsers_qute # browser with VIM handles
}
javadev () {
    bash $scriptPath/~javadev.sh
    #install_with_feedback maven #~36MB of extra packages with this system
    var_javadev=true
}
javadev_gui () {
    bash $scriptPath/~javadev_gui.sh
}
dev_tools_vim () {
    install_with_feedback vim #Note that vi is installed by default, but vim is easier to use
    git clone https://github.com/chriskempson/base16-shell.git ~/.config/base16-shell
    echo "BASE16_SHELL=\$HOME/.config/base16-shell/" | sudo tee --append ~/.bashrc
    echo "[ -n \"\$PS1\" ] && [ -s \$BASE16_SHELL/profile_helper.sh ] && eval \"\$(\$BASE16_SHELL/profile_helper.sh)\"" | sudo tee --append ~/.bashrc
    # To-do
    # Write these instructions
    # https://github.com/jarolrod/vim-python-ide

    install_with_feedback libreoffice-script-provider-python # Eg highlight code
    pip3_install pygments
    cd /tmp
    curl -s https://api.github.com/repos/slgobinath/libreoffice-code-highlighter/releases/latest | egrep -o 'http.*\.oxt\.zip*' | wget -i - -O codehighlighter.oxt.zip
    unzip codehighlighter.oxt.zip
    rm codehighlighter.oxt.zip
}
dev_tools_nice () {
    echo 'Empty function: dev_tools_nice'
}
android () {
    install_with_feedback mtpfs jmpfs jmtpfs mtp-tools gmtp
}
android_root () {
    install_with_feedback android-tools* adb fastboot
}
media_tools () {
    install_with_feedback ffmpeg musepack-tools wavpack id3 vorbis-tools mppenc lame faac faad flac mikmod
    install_with_feedback libav-tools # Can use to painlessly cut video files, e.g. avconv -ss hh:mm:ss (start) -i input.webm  -t hh:mm:ss (duration) -codec: copy output.webm
    install_with_feedback -plugins
}    
media_editing () {
    install_with_feedback audacity gimp
}
yepkit () {
    # This is a controller for a specific programmable USB hub
    install_with_feedback libusb-1.0-0-dev libusb-1.0-0
    install_with_feedback libusb-1.0 g++ # Unlisted prerequisite
    install_with_feedback pkg-config libhidapi* libudev* # Probably overkill. Some unlisted prerequisites.
    cd /tmp
    git clone https://www.github.com/Yepkit/ykush
    sudo build.sh # Often needs prerequisites that otherwise cause errors
    sudo build.sh # IIRC, running again fixes the errors
    sudo build.sh # Just in case
    sudo install.sh
}
misc () {
    echo 'Empty function: misc'
    #install_with_feedback obs-studio # Recording screen and streaming
    #yepkit
    #cd /run
    #sudo git clone https://github.com/butscher/WikidPad # python /run/WikidPad/WikidPad.py. Only for GNOME.
}
vpn () {
    install_with_feedback openvpn network-manager-openvpn dnscrypt-proxy nmap
}
privacy_core () {
    vpn
}
privacy () {
    install_with_feedback libimage-exiftool-perl #Remove EXIF data from photos
    var_privacy=true
}
network_hosts () {
    while IFS= read -r line; do
        printf "$line"
        echo "$line" | sudo tee --append /etc/hosts
    done < "$scriptPath/hosts"
    install_with_feedback nscd
    sudo service nscd restart # Effect our hosts file changes
}
network_mac_change () {
    #sudo cp ~00-default.link /etc/systemd/network/00-default.link
    #sudo service systemd-networkd restart
    #sudo service systemd-resolved restart
    install_with_feedback macchanger
}
network_advanced () {
    echo 'Empty function: network_advanced'
}
zulucrypt_core () {
    install_with_feedback zulucrypt-cli zulumount-cli
}
encrpytion () {
    zulucrypt_core
}
python_core () {
    install_with_feedback python-pip python3-pip python3-dev
    pip3_install re # RegEx
    var_python3_core=true
}
python3_thefuck () {
    pip3_install thefuck
}
python3_webget () {
    pip3_install beautifulsoup4 requests
}
python3_webget_selenium () {
    #pip3_install 
    echo 'Empty function: python3_webget_selenium'
}
python3_maths () {
    pip3_install scipy pandas itertools
}
python3_lang () {
    pip3_install goslate nltk
}
python3_nice () {
    pip3_install prettytable
}
python_autoGUI () {
    pip_install pyautogui
    pip_install scrot # Screenshot functions for pyAutoGUI
    pip_install python3-xlib
    #pip3_install steamed # For Steam sales, e.g. steamed.sales() returns array of games on sale
}
python3_misc () {
    pip3_install shlex # RegEx
    pip3_install itertools
    pip3_install readline
    pip3_install glob
}
python_audio_textToSpeech () {
    #install_with_feedback gnustep-gui-runtime
    install_with_feedback libttspico-utils # this is pico2wave
    # Usage example:
    # pico2wave -l en-GB -w "/tmp/WAV.wav" "Next playlist" && vlc "/tmp/WAV.wav"
}
python_audio () {
    install_with_feedback python-alsaaudio python-pygame
    python_audio_textToSpeech
    #pip_install playsound
}
python_gamedev () {
    install_with_feedback renpy
}
caffe_install () {
    if [ "$var_caffe_install" = true ]; then
        echo 'Caffe already installed'
    else
        if [ "$var_nvidia_cuda" = true ]; then
            install_with_feedback caffe-cuda
        else
            install_with_feedback caffe-cpu
        fi
        var_caffe_install=true
    fi
}
torch_install () {
    if [ "$var_torch_install" = true ]; then
        echo 'Torch already installed'
    else
        mkdir -p ~/bin/tools/neural-networks
        git clone https://github.com/torch/distro.git ~/bin/tools/neural-networks/torch --recursive
        cd ~/bin/tools/neural-networks/torch
        bash install-deps
        ./install.sh
        var_torch_install=true
    fi
    install_with_feedback luarocks
    luarocks install cudnn
}
python_neuralnets () {
    pip_install tensorflow-gpu pillow
    install_with_feedback libcupti-dev
    python_neuralnets_styleTransfer
    bash $anyBuntuPath/deep-learning.sh
    bash $anyBuntuPath/~deep-learning.sh
}
python3_neuralnets () {
    pip3_install tensorflow-gpu
}
python_neuralnets_styleTransfer () {
    cd /tmp
    git clone https://github.com/titu1994/Neural-Style-Transfer neural-style-transfer
    mkdir -p ~/bin/tools/tensorflow
    mv neural-style-transfer ~/bin/tools/tensorflow
    pip_install keras h5py
    if [ "$var_nvidia_cuda" = true ]; then
        python_tensorflow_gpu
    else
        python_tensorflow_cpu
    fi
}
python_tensorflow_cpu () {
    install_with_feedback python-virtualenv
    mkdir -p ~/bin/tools/tensorflow_cpu
    cd ~/bin/tools/tensorflow
    virtualenv --system-site-packages -p python .
    source bin/activate
    pip_install tensorflow
    deactivate
}
python3_tensorflow_cpu () {
    install_with_feedback python3-virtualenv
    mkdir -p ~/bin/tools/tensorflow3_cpu
    cd ~/bin/tools/tensorflow3_cpu
    virtualenv --system-site-packages -p python3 .
    source bin/activate
    pip3_install tensorflow
    deactivate
}
python_tensorflow_gpu () {
    install_with_feedback python-virtualenv
    mkdir -p ~/bin/tools/tensorflow_gpu
    cd ~/bin/tools/tensorflow
    virtualenv --system-site-packages -p python .
    source bin/activate
    pip_install tensorflow_gpu
    deactivate
}
python3_tensorflow_gpu () {
    install_with_feedback python3-virtualenv
    mkdir -p ~/bin/tools/tensorflow3_gpu
    cd ~/bin/tools/tensorflow3_gpu
    virtualenv --system-site-packages -p python3 .
    source bin/activate
    pip3_install tensorflow_gpu
    deactivate
}
video_wallpaper () {
    mkdir ~/.local/share/plasma
    mkdir ~/.local/share/plasma/wallpapers
    git clone https://github.com/halverneus/org.kde.video.git ~/.local/share/plasma/wallpapers/org.kde.video #~/.local/share/plasma/wallpapers/org.kde.video.git
}
gaming () {
    install_with_feedback steam
}
gaming_emulation () {
    echo "WINE" # requires custom repo
    sudo dpkg --add-architecture i386 #enables 32 bit architecture
    cd /tmp
    wget -nc https://dl.winehq.org/wine-builds/Release.key
    sudo apt-key add Release.key
    install_with_feedback --install-recommends winehq-stable
    var_gaming_emulation=true
}
gaming_emulation_gui () {
    winecfg
}
virtual_machines () {
    install_with_feedback qemu-kvm libvirt-bin ubuntu-vm-builder bridge-utils virt-manager libusbredirparser-dev
    sudo adduser `id -un` libvirtd # After this, you need to relogin so that your user becomes an effective member of the libvirtd group. This is done last.
    var_virtual_machines=true
}
virtual_machines_gui () {
    printf '\n\n\nRe-login to configure libvirtd group for KVM virtual machine'
    printf 'Instructions on how to proceed will be left in /tmp/instructions'
    echo 'follow the steps outlined here: https://help.ubuntu.com/community/KVM/Installation' >> /tmp/instructions
    echo 'starting from:' >> /tmp/instructions
    echo '    "After this, you need to relogin so that your user becomes an effective member of the libvirtd group. The members of this group can run virtual machines."' >> /tmp/instructions
}
ssh_tools () {
    install_with_feedback net-tools openssh-server
}
3d_modelling_blender () {
    install_with_feedback blender
    cd /tmp
    git clone https://github.com/makehumancommunity/community-plugins
    sudo mv -r makehuman /usr/share/makehuman/plugins
    git clone https://github.com/sobotka/filmic-blender # Filmic Colour Management
    cd /usr/share/blender/*.*
    sudo mv datafiles datafiles_old
    sudo mv /tmp/filmic-blender ./datafiles
    install_with_feedback nvidia-modprobe # nvidia-cuda-toolkit # allows Blender to detect your nvidia CUDA device (to render via GPU) # Almost 2 GB!!!
}
3d_modelling_gamedev () {
    var_3d_modelling_gamedev=true
}
3d_modelling_gamedev_gui () {
    firefox https://forum.unity.com/threads/unity-on-linux-release-notes-and-known-issues.350256/page-2
}
3d_modelling () {
    3d_modelling_blender
    install_with_feedback makehuman
}
filesharing () {
    install_with_feedback nautilus samba cifs-utils #samba-common python-dnspython #already included
}
nvidia_driver () {
    #sudo apt purge nvidia-* # Purges previously installed nvidia drivers # Don't do this yet - we seem to have to run the installation script more than once...
    #install_with_feedback "nvidia-$latestNVidiaDriver"
    sudo ubuntu-drivers autoinstall
    var_nvidia_driver=true
}
nvidia_cuda () {
    if [ "$var_nvidia_cuda" = true ];
        echo 'NVidia CUDA already installed'
    else
        install_with_feedback linux-headers-$(uname -r)
        install_with_feedback cuda-$(echo $cudaVersion | tr . -) # cuda-9-1 # Replace . with -
        for d in /usr/local/cuda-$(echo $cudaVersion | tr . -)*; do
            export PATH=$PATH:/usr/local/$d
            echo PATH=\$PATH:/usr/local/$d | sudo tee --append ~/.bashrc
        done
        var_nvidia_cuda=true
    fi
}
nvidia_driver_fix () {
    printout " gksu (Graphical SU) -y install fix for NVidia"
    #NVidia drivers currently cause problems for it, so this fixes it"
    #Source: Noisy_Botnet from askubuntu 2017"
    #We call _onNVidiaDriverUpdate.sh to do this"
    sudo chmod +x _onNVidiaDriverUpdate.sh
    bash _onNVidiaDriverUpdate.sh
}
dev_tools () {
    install_with_feedback vim nodejs
    #install_with_feedback sqlitebrowser
}
things_kde () { # Purpose unknown, almost certainly required for some packages
    install_with_feedback kdesrc-build libkf5crash-dev libkf5filemetadata-dev python-kde4
}
things_qt () {
    install_with_feedback libqt5xmlpatterns5-dev qt*5-dev
}
bluetooth_resolve_issues () {
    systemctl --user enable obex
    # The temproary solution is: systemctl --user start obex
    #install_with_feedback qml-modules-org-kde-purpose # ??
    install_with_feedback blueman
}
overclocking_tweaks () {
    mkdir -p ~/bin/tools/overclocking
    cd /tmp
    curl https://github.com/nan0s7/nfancurve/releases | grep .tar.gz
    read -p "Latest release: v" latestRelease
    wget https://github.com/nan0s7/nfancurve/archive/v$latestRelease.tar.gz
    tar xvzf v$latestRelease.tar.gz
    touch fan-curve.desktop
    echo '[Desktop Entry]' >> fan-curve.desktop
    echo 'Name=nan0s7_fan-curve_script' >> fan-curve.desktop
    echo 'Type=Application' >> fan-curve.desktop
    echo "Path=$HOME/bin/tools/overclocking/" >> fan-curve.desktop
    echo "Path=$HOME/bin/tools/overclocking/temp.sh" >> fan-curve.desktop
    sudo mkdir -p ~/.config/autostart
    mv fan-curve.desktop ~/.config/autostart/fan-curve.desktop
}
google_to_duckduckgo () {
    # Redirects google.com to duckduckgo.com
    # Just for laughs
    # You probably don't want this
    host duckduckgo.com | grep " has address " >> /tmp/ddg_ips # output is lines like "duckduckgo.com has address 123.45.678.9
    ddg_ip_line=$(tail -1 /tmp/ddg_ips)
    ddg_ip=${gccVersionText##* } # Extract text before whitespace
    echo "# Google to DuckDuckGo" | sudo tee --append /etc/hosts
    echo "$ddg_ip google.com" | sudo tee --append /etc/hosts
    echo "$ddg_ip google.co.uk" | sudo tee --append /etc/hosts
}
dns_to_custom_ethernet () { # User input required
    # Find the IDs of our networks
    ethernet_str=$(nmcli c show | grep ethernet)
    read -p "Ethernet connection name:    " ethernet_main
    #ethernet_main=${ethernet_str%% *} # Get ID of the ethernet connection in use # Because some ethernets are not called eth1 apparently
    echo "You probably have IP address $(ip addr show | grep "inet" | grep -v inet6 | grep -v vir | grep -v 127.0.0.1)"
    read -p "Your IP address:    " my_ip
    nmcli c modify "$ethernet_main" ipv4.addresses $my_ip
    nmcli c modify "$ethernet_main" ipv4.method manual
    nmcli c modify "$ethernet_main" ipv6.method ignore
    nmcli c modify "$ethernet_main" ipv4.dns $dns_ip
}
dns_to_custom_wifi () { # User input required
    # Find the IDs of our networks
    ethernet_str=$(nmcli c show | grep wireless)
    read -p "Wireless connection name:    " ethernet_main
    echo "You probably have IP address $(ip addr show | grep "inet" | grep -v inet6 | grep -v vir | grep -v 127.0.0.1)"
    read -p "Your IP address:    " my_ip
    nmcli c modify "$ethernet_main" ipv4.addresses $my_ip
    nmcli c modify "$ethernet_main" ipv4.method manual
    nmcli c modify "$ethernet_main" ipv6.method ignore
    nmcli c modify "$ethernet_main" ipv4.dns $dns_ip
}
grub_menu_nonzero_hidden_timeout () {
    sudo sed -i 's/GRUB_HIDDEN_TIMEOUT=0/GRUB_HIDDEN_TIMEOUT=2/g' /etc/default/grub # Replace
    sudo update-grub2
    sudo update-grub
}

mine_musicoin () { # MusicCoin
    cd /tmp
    curl -s https://api.github.com/repos/musicoin/go-musicoin/releases/latest | egrep -o 'http.*\linux-amd64' | wget -i - -O gmc
    sudo chmod +x gmc
    sudo cp gmc /usr/bin
    var_musicoin=true
}

#
#
#
# Start of no user interaction

parentdir="$(dirname "$PWD")" #gets parent directory of current working directory #quotes inside and out are important, else risk losing data


if ! [ -d "$HOME/bin" ]; then
    echo "Copying Scripts over..."
    cp "$parentdir" "$HOME/bin"
else
    echo "$HOME/bin already exists"
fi

#No need to wait for copying; the apt -y installs will take more than enough time



printout " Keep-alive: update existing sudo time stamp if set, otherwise do nothing."
#Credit: cowboy (GIThub) aka Gregory Perkins serverFault.com
while true; do sleep 60; sudo -n true; kill -0 "$$" || exit; done 2>/dev/null &

printout "Adding Repositories"
add_repository ppa:nilarimogard/webupd8 # SWF player
add_repository ppa:obsproject/obs-studio # OpenBroadcaster
add_repository ppa:pythonxy/pythonxy-stable # Python-xy
add_repository https://dl.winehq.org/wine-builds/ubuntu/ # WINE
#add_repository ppa:ubuntu-desktop/ubuntu-make # IDE for Python (make)
add_repository ppa:makehuman-official/makehuman-11x
add_repository ppa:thomas-schiex/blender
add_repository ppa:ubuntu-mozilla-daily/ppa
#cd /tmp
#wget http://archive.getdeb.net/install_deb/getdeb-repository_0.1-1~getdeb1_all.deb # For WikidPad, but lots of other stuff here # Wikidpad only for Ubuntu 16.04
#sudo dpkg -I getdeb-repository_0.1-1~getdeb1_all.deb
sudo apt -y update

#printout 'Debsums: checksums for packages, particularly useful for finding modified config files'
#install_with_feedback debsums

#install_with_feedback Xvfb printf # Unknown
#performs all graphical operations in virtual memory without showing any screen output


#Font editor
#install_with_feedback fontforge


#install_with_feedback kodi kodi-bin







#printout 'IDE for Python'
#install_with_feedback ubuntu-make # requires custom repo
# Requires human interaction for last part, so that part is at the bottom




# It don't work
#printout 'ISP Data Pollution/Obfuscation' # github.com/essandess/isp-data-pollution
#cd /tmp
#git clone https://github.com/essandess/isp-data-pollution.git
#cd isp-data-pollution
#pip3_install faker openssl
##install_with_feedback fontconfig libfontconfig build-essential chrpath libssl-dev libxft-dev libfreetype6 libfreetype6-dev libfontconfig1 libfontconfig1-dev
##export PHANTOM_JS="phantomjs-2.1.1-linux-x86_64"
##sudo mv $PHANTOM_JS /usr/local/share
##sudo ln -sf /usr/local/share/$PHANTOM_JS/bin/phantomjs /usr/local/bin



#
#
#
# Categories
install_category_core () { ## CORE (Depended on in the script; sometimes even depend on each other)
    #hdds_keep_running # No dependencies # NB: requires user interaction
    install_with_feedback ubuntu-restricted-extras # NB: requires user interaction at the start
    sudo apt -y full-upgrade
    env_var # Probably not important in this script
    #ramdisk_temporary # IMPORTANT! Must be first in order
    ramdisk_permanent # IMPORTANT! Must be first in order
    nvidia_driver
    #nvidia_driver_fix
    installation_tools # IMPORTANT! Has dependencies
    webget_core # IMPORTANT! Has dependencies
    things_kde
    things_qt
}
install_category_common () {
    ## Common
    nice_to_haves
    ssh_tools
    media_players
    media_tools
    webget
    browsers
    android
    misc
}
install_category_dev () {
    ## Dev
    javadev
    dev_tools
    dev_tools_nice
    git_dev
    git_nice
}
install_category_nice () {
    ## Nice
    video_wallpaper
}
install_category_privacy () {
    privacy
    privacy_core
}
install_category_network () {
    ## Network
    network_hosts
    network_advanced
    #filesharing
    #android_root
    #bluetooth_resolve_issues
    var_dns_to_custom_ethernet=true
    var_dns_to_custom_wifi=true
}
install_category_python_core () {
    ## Python CORE
    python_core
    python3_maths
}
install_category_python_webget () {
    python3_webget
    python3_webget_selenium
    python_audio
}
install_category_python_neuralnets () {
    nvidia_cuda # Must be first, since following have options for CPU, GPU, or GPU_CUDA
    caffe_install
    torch_install
    python_neuralnets
    #python3_neuralnets
}
install_category_neuralnets () {
    nvidia_cuda # Must be first, since following have options for CPU, GPU, or GPU_CUDA
    caffe_install
    torch_install
}

#
#
# Mega-categories
install_megacategory_python () {
    install_category_python_core
    install_category_python_webget
    install_category_python_neuralnets
    #python_gamedev
    #python3_lang
    #python3_misc
    #python_autoGUI
}
add_functions_to_usr_bin () {
    cd $bashPath
    echo "export PATH=\$PATH:/usr/bin/custom" | sudo tee --append ~/.bashrc
    sudo mkdir /usr/bin/custom
    for g in */*/*.sh; do
        f=$(echo "$g" | tr "~" _) # Replace ~ with _
        filename=${f##*/}
        filename_original=${g##*/}
        basename=${filename%%.*}
        sudo cp "$g" /usr/bin/custom
        sudo mv /usr/bin/custom/"$filename_original" /usr/bin/custom/"$filename"
        sudo chmod u+x /usr/bin/custom/"$filename"
        echo "$basename () { bash $basename.sh "$@"; }; export -f $basename" | sudo tee --append ~/.bashrc # WARNING! No spaces in filenames # "$@" passes on arguments to the bash script called by the function. It is handled in a special way, it is not parsed as a single string.
    done
    for g in */*.sh; do
        f=$(echo "$g" | tr "~" _) # Replace ~ with _
        filename=${f##*/}
        filename_original=${g##*/}
        basename=${filename%%.*}
        sudo cp "$g" /usr/bin/custom
        sudo mv /usr/bin/custom/"$filename_original" /usr/bin/custom/"$filename"
        sudo chmod u+x /usr/bin/custom/"$filename"
        echo "$basename () { bash $basename.sh "$@"; }; export -f $basename" | sudo tee --append ~/.bashrc # WARNING! No spaces in filenames
    done
    for g in *.sh; do
        f=$(echo "$g" | tr "~" _) # Replace ~ with _
        filename=${f##*/}
        filename_original=${g##*/}
        basename=${filename%%.*}
        sudo cp "$g" /usr/bin/custom
        sudo mv /usr/bin/custom/"$filename_original" /usr/bin/custom/"$filename"
        sudo chmod u+x /usr/bin/custom/"$filename"
        echo "$basename () { bash $basename.sh "$@"; }; export -f $basename" | sudo tee --append ~/.bashrc # WARNING! No spaces in filenames
    done
    source ~/.bashrc # So that functions are immediately available
}
