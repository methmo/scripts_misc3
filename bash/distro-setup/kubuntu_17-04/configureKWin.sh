kcmshell5 kwindecoration --caption "TitlebarMenu:->Buttons->Drag 3 horiz. lines icon to titlebar"
kcmshell5 style --caption "TitlebarMenu:->FineTuning->MenubarStyle to TitlebarButton"
kcmshell5 kcm_lookandfeel --caption "Select Look and Feel"
kcmshell5 kcm_splashscreen --caption "Select splashscreen"


kcmshell5 kcmshell5 kcm_desktoptheme --caption "Get Diamond" # Desktop Themes
kcmshell5 kcmshell5 kcm_sddm --caption "Change Login wallpaper"

kcmshell5 desktoppath --caption "Location for personal files"
kcmshell5 filetypes --caption "File associations"
kcmshell5 kcm_activities

kcmshell5 screenlocker --caption "Set video logout wallpaper"
