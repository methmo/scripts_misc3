printf 'Firefox Config'
cd ~/.mozilla/firefox/*.default
mkdir chrome
cd chrome
touch userContent.css
echo '@-moz-document url("about:home"), url("about:newtab") {
    .activity-stream {
        background: #112230 !important;
    }
}' >> userContent.css
