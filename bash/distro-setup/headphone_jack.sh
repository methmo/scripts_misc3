# Credit to Stunts from https://askubuntu.com/questions/521371/how-to-pause-vlc-playback-when-the-headphones-are-disconnected in 2015

_pid=`pgrep vlc`
_pid="${_pid% *}"
DBUS_SESSION_BUS_ADDRESS=`grep -z DBUS_SESSION_BUS_ADDRESS /proc/$_pid/environ | sed -e 's/DBUS_SESSION_BUS_ADDRESS=//'`
_username=`grep -z USER= /proc/$_pid/environ |sed 's/.*=//'`

if [ "$3" = unplug ]; then
    su $_username -c "DBUS_SESSION_BUS_ADDRESS=$DBUS_SESSION_BUS_ADDRESS dbus-send --print-reply --session --dest=org.mpris.MediaPlayer2.vlc /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Pause"
else
    su $_username -c "DBUS_SESSION_BUS_ADDRESS=$DBUS_SESSION_BUS_ADDRESS dbus-send --print-reply --session --dest=org.mpris.MediaPlayer2.vlc /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Play"
fi
