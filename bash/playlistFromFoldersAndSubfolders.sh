#Author: Adam Gray 2017
#Lists all those mp3s in the directory (NOT subdirs!) in a playlist, for each directory. So eg DeutschSprachige does not include DeutschSprachigeMarchieren
#echo "Running script..."
shopt -s globstar
#echo "Enabling globstar..."
read -p 'This script saves music tree HERE to playlist lists in ANOTHER directory' dummy # CTRL+C out if not in right directory
read -p '/Output/directory:    ' outDir
origDir="$PWD"

mkdir $outDir
cd "$origDir"
for i in **/; do
  cd "$i"
  removeTrailingSlash=${i%/}
  replaceOtherSlashes=$(echo "$removeTrailingSlash" | tr "/ " "__")
  ls "${PWD}/"*.mp3 > "$outDir"/"$replaceOtherSlashes".m3u
  ls "${PWD}/"*.mp3 > "$outDir"/"$replaceOtherSlashes"_incSubdirs.m3u
  
  parentDirPath="$(dirname "$i")"
  removeTrailingSlashParent=${parentDirPath%/}
  replaceOtherSlashesParent=$(echo "$removeTrailingSlashParent" | tr "/ " "__")
  ls "${PWD}/"*.mp3 >> "$outDir"/"$replaceOtherSlashesParent"_incSubdirs.m3u
  
  cd "$origDir"
done
