#!/bin/bash

read -p "Commit ID:    " commit_id
read -p "Filename:    " filename

git checkout -b temp "$commit_id"
git rm "$filename"
git commit --ammend --no-edit
git rebase --preserve-merges --onto temp "$commit_id" master
git diff master@{1} # Check for unexpected changes

# On the off chance that you accidentally dox yourself...
# And my gosh! Be damn careful. Copy-paste the wrong thing, forget to "git rebase --abort", and it gets painful to repair...

# Credit: user456814 from stackoverflow.com from 2014
