#!/bin/bash

wgetu () {
    cd /tmp
    mcdir wgetu
    wget_arguments=$(list-tabs "$1")
    mkdir -p ~/bin/vars
    if [ -f ~/bin/vars/wget_sites ]; then
        echo ""
    else
        touch ~/bin/vars/wget_sites
    fi
    wget_sites=$(cat ~/bin/vars/wget_sites)
    python3 -c "
import subprocess
    
wget_arguments='$wget_arguments'.split('\n')
    
d={}
with open('$HOME/bin/vars/wget_sites', 'r+') as f:
    for row in f:
        columns=row.split(',')
        site=columns[0]
        agent=columns[1]
        d[site]=agent
    
    for url in wget_arguments:
        if url.strip()!='':
            site_downloaded=0
            for site in d:
                if site in url:
                    agent=d[site]
                    subprocess.call([agent,url])
                    site_downloaded=1
            if site_downloaded==0:
                domain=url.split('//')[-1].split('www.')[-1].split('/')[0]
                print('Domain not configured for '+domain)
                agent=input('Agent for {0} (e.g. wget):    '.format(domain))
                f.write('{0},{1}'.format(domain,agent))
                d[domain]=agent
                subprocess.call([agent,url])
    f.close()
"
}
