#!/bin/bash

# Credit to Robert Siemer from StackOverflow in 2015 for format of getopt useage

OPTIONS=pwylfvh
LONGOPTIONS=process,wget,ytdl,list,list-to-file,verbose,help

# -temporarily store output to be able to check for errors
# -e.g. use “--options” parameter by name to activate quoting/enhanced mode
# -pass arguments only via   -- "$@"   to separate them correctly
PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTIONS --name "$0" -- "$@")
if [[ $? -ne 0 ]]; then
    # e.g. $? == 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
fi
# read getopt’s output this way to handle the quoting right:
eval set -- "$PARSED"

# now enjoy the options in order and nicely split until we see --
while true; do
    case "$1" in
        -p|--process)
            p=true
            shift
            ;;
        -w|--wget)
            w=true
            shift
            ;;
        -y|--ytdl)
            y=true
            shift
            ;;
        -l|--list)
            l=true
            shift
            ;;
        -f|--list-to-file)
            f=true
            shift 2
            ;;
        -v|--verbose)
            v=true
            shift
            ;;
        -h|--help)
            h=true
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

# handle non-option arguments
if [[ $# -eq 0 ]]; then
    searchString=""
elif [[ $# -eq 1 ]]; then
    searchString="$1"
else
    echo "Sorry, wgetu does not currently support multiple strings, because I am lazy. It will soon, maybe."
    exit 4
fi










wgetu_process () {
    cd /tmp
    mcdir wgetu
    wget_arguments=$(list-tabs "$searchString")
    mkdir -p ~/bin/vars
    if [ -f ~/bin/vars/wget_sites ]; then
        echo ""
    else
        touch ~/bin/vars/wget_sites
    fi
    wget_sites=$(cat ~/bin/vars/wget_sites)
    python3 -c "
import subprocess
    
wget_arguments='$wget_arguments'.split('\n')
    
d={}
with open('$HOME/bin/vars/wget_sites', 'r+') as f:
    for row in f:
        columns=row.split(',')
        site=columns[0]
        agent=columns[1]
        url_post=columns[2]
        d[site]=[agent,url_post]
    
    for url in wget_arguments:
        if url.strip()!='':
            site_downloaded=0
            for site in d:
                if site in url:
                    agent=d[site][0]
                    subprocess.call([agent,url])
                    site_downloaded=1
            if site_downloaded==0:
                domain=url.split('//')[-1].split('www.')[-1].split('/')[0]
                print('Domain not configured for '+domain)
                agent=input('Agent for {0} (e.g. wget):    '.format(domain))
                url_post=input('Append to urls of this kind:    ')
                f.write('{0},{1},{2}'.format(domain,agent,url_post))
                d[domain]=[agent,url_post]
                if url[-1]=='/':
                    url=url[:-1]
                subprocess.call([agent,url[:-1]+url_post]) # [:-1] to remove trailing /
    f.close()
"
}











if [ "$h" = true ]; then
    printf "
-p,--process        Automatically assign agents to deal with each url,
                    depending on the domain,
                    as set in ~/bin/vars/wget_sites
-w,--wget
-y,--ytdl           youtube-dl all urls
-l,--list           print all urls
-f,--list-to-file   /tmp/tabs
-v,--verbose
-h,--help
"
else
    if [ "$p" = true ]; then
        wgetu_process
    else
        if [ "$w" = true ]; then
            wget $(list-tabs "$searchString") # This is a string of urls seperated by whitespaces
        else
            if [ "$y" = true ]; then
                youtube-dl $(list-tabs "$searchString") # This is a string of urls seperated by whitespaces
            else
                if [ "$f" = true ]; then
                    list-tabs "$searchString" | tr " " "\n" >> /tmp/tabs # This is a string of urls seperated by newlines
                else
                    echo "No mode specified, so defaulting to list"
                    echo "Search string: '$searchString'"
                    list-tabs "$searchString" | tr " " "\n" # This is a string of urls seperated by newlines
                fi
            fi
        fi
    fi
fi
