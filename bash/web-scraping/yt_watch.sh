#!/bin/bash

# Bypasses youtube nonsense

cd /tmp
mkdir yt_watch
cd yt_watch
youtube-dl "$@" # All arguments; @ is handled a special way, so is not parsed as one single string
latestFile=$(ls -t | head -n1)
see "$latestFile"
