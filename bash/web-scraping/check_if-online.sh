#!/bin/bash

domain_a=duckduckgo.com
domain_b=reddit.com

if ping -c 1 "$domain_a" >> /dev/null 2>&1; then
    echo "online"
else
    if ping -c 1 "$domain_b" >> /dev/null 2>&1; then
        echo "online"
    else
        echo "offline"
    fi
fi
