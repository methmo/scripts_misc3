#!/bin/bash

# Credit: wbob from stackexchange.com, August 2017

#echo 'Usage: ./list-tabs.sh [CONTAINING]'

opentabs=$(find ~/.mozilla/firefox*/*.default/sessionstore-backups/recovery.jsonlz4 -print -quit); # print quit makes it get first result only

#read -p "List tabs containing:    " toRemove
#read -p "List neighbouring url? [blank=no]:    " printLastUrl
toRemove="$1"
printLastUrl=""
python3 <<< $"
import os, sys, json, lz4, subprocess

target_contents = ['$toRemove'.lower()]
if target_contents[0] == 'media':
    target_contents = ['.jpg','.jpeg','.png','.tiff','.gif','.webm','.mp4']
elif target_contents[0] == 'audio':
    target_contents = ['.mp3','.ogg','.webm']
print_last_url = '$printLastUrl'
last_url = ''
toOpen = []

print_at_end=''
def printadd(to_print): # Spaces are more convenient for our use case
    return print_at_end+' '+to_print
def translate_url(url):
    if ('.reddit.com/r/' in url)&('/comments/' in url)&(url[-1]=='/'): # Last one checks if .json
        return url[:-1]+'.json'
    if ('gfycat.com' in url)&('giant.gfycat.com' not in url):
        return url.replace('gfycat.com','giant.gfycat.com')+'.webm'

f = open('$opentabs', 'rb')
magic = f.read(8)
jdata = json.loads(lz4.block.decompress(f.read()).decode('utf-8'))
f.close()
for win in jdata.get('windows'):
    for tab in win.get('tabs'):
        try:
            i = tab.get('index') - 1
            urls = tab.get('entries')[i].get('url')
            urls = translate_url(urls)
            is_target=0
            for target in target_contents:
                if target in urls:
                    is_target=1
            if is_target:
                if (last_url!='')&(print_last_url!=''):
                    print_at_end=printadd(last_url)
                print_at_end=printadd(urls)
            else:
                last_url=urls
        except:
            pass
print(print_at_end)
"
