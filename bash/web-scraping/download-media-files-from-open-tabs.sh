# Credit: wbob from stackexchange.com, August 2017

pip3 install simplejson || sudo !!
pip3 install lz4 || sudo !!

export opentabs=$(find ~/.mozilla/firefox*/*.default/sessionstore-backups/recovery.jsonlz4 -print -quit); # print quit makes it get first result only

#read -p 'Mediums:    ' mediums_inp

python3 <<< $"
import os, sys, json, lz4, subprocess

accounts={}
accounts['hegre.com']=['badumptao','fadoopEy']

mediums = ['mp4', 'mp3', 'ogg', 'png', 'jpg', 'jpeg', 'gif', 'gifv']
mediums_inp = '$mediums_inp'.strip().lower()
if mediums_inp!='':
    mediums=mediums_inp.split()

f = open(os.environ['opentabs'], 'rb')
magic = f.read(8)
jdata = json.loads(lz4.block.decompress(f.read()).decode('utf-8'))
f.close()
for win in jdata.get('windows'):
    for tab in win.get('tabs'):
        try:
            i = tab.get('index') - 1
            urls = tab.get('entries')[i].get('url')
            for media in mediums:
                if '.'+media in urls:
                    preUrl = urls.split('.'+media)[0]
                    webpath = preUrl + '.'+media # removes any trailing bit like &var=1
                    
                    domained=False
                    for domain in accounts:
                        print(domain)
                        if domain in preUrl:
                            usr=accounts[domain][0]
                            pwd=accounts[domain][1]
                            domained=True
                            subprocess.call(['wget','-v','--user='+usr,'--password='+pwd,webpath])
                    if not domained:
                        subprocess.call(['wget',webpath])
        except:
            pass#print(tab)
"
