# win10-wallpapers
A Windows 10 project to automatically change the desktop theme, set a playlist of mp4 files as the wallpaper using VLC, and play a specific playlist.

It is an old project of mine, and almost entirely utilising batch, so the code is _extremely_ messy, and having moved to Linux, I will almost certainly not be cleaning it up. It is here for the microscopic chance that somebody unfortunate enough to be learning batch might find something here useful, amongst all the mess.

# Details
The convoluted BAT file generates two groups of files (one for a muted video wallpaper, one not muted) for each subdirectory of a given directory on D: drive containing video files, along with a CSV which allocates a desktop theme and music playlist to each subdirectory.

Each group has two subgroups of files: hidden (no CMD prompt visible) and not hidden (a CMD prompt visible while the script runs). Each subgroup contains a BAT file to set the wallpaper to a video playlist, using VLC, and (if video is muted) play a custom music playlist, and set the desktop theme.

Each of the BAT files relies on the VBS script to automatically close the personalisation window that pops up when we change the theme.

# To Use
I have removed the arguments I used for calling VLC, since I did not write them; these can easily be found by searching online for 'windows 10 VLC dreamscene', although you will have to adjust the arguments according to your screen and display settings.

The BAT files generated are called '...LeftScreen...' because the arguments I used only applied to the left screen (this is the default if you only have one screen). It would not be difficult to edit this to generate scripts that change other screens to different (or the same) video wallpaper playlists.

Those using US English should edit the contents of the VBS script to use the American English "Personalization" rather than "Personalisation".

# Features
Limited error messages.

# Possible Issues
If we close the personalisation window immediately after opening the .theme file, Windows doesn't change the theme. So we add a timeout between these. You might have to modify the length of the timeout depending on your system.

For me, icons on the desktop do not appear above the video wallpaper. This can probably be solved by adding an extra argument when calling VLC.

I have hurriedly made some corrections to the file (removing excessive debugging info), but am currently on a Linux, so I haven't had a chance to check if I have broken it or not.
