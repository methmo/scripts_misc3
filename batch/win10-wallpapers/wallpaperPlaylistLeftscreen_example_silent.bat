@echo off 
set _InputFile="wallpaper_CSVVideoThemeMusic.txt" 
setlocal EnableDelayedExpansion 
if not exist %_InputFile% ( 
  echo. 
  echo. 
  echo ERROR! Or false positive. ! 
  echo CSV file %_InputFile% does not exist. Rerun the rebuildCSV version of the wallpaperPlaylistLeftscreen__createBAT BAT file 
  pause 
  exit 
) 
for /f "tokens=1-3 delims=," %%A in (%_InputFile%) do ( 
  if "%%~A"=="MusicVideos_silent" ( 
    "C:\Users\Adam\AppData\Local\Microsoft\Windows\Themes\%%~B.theme" 
    timeout 2 
    if not exist "wallpaper_closePersonalisationWindow.vbs" ( 
      echo. 
      echo. 
      echo ERROR! Or false positive. ! 
      echo "wallpaper_closePersonalisationWindow.vbs" does not exist or has been moved. This isnt actually a problem for the script. 
      echo If you want to run the script and cannot find a copy of this file, just delete the lines of script that mention it. 
      pause 
      exit 
    ) 
    start "" ""wallpaper_closePersonalisationWindow.vbs"" 
	 if not "%%~C" == "" ( 
      if not exist "wmpPlaylist_%%~C.bat" ( 
        echo. 
        echo. 
        echo ERROR! Or false positive. ! 
        echo "wmpPlaylist_%%~C.bat" does not exist or has been moved or deleted. 
        pause 
        exit 
      ) 
      cmd /c "wmpPlaylist_%%~C.bat" 
    ) 
  ) 
) 
taskkill /F /IM vlc.exe 
d: 
if not exist D:\Videos\example ( 
  echo. 
  echo. 
  echo ERROR! Or false positive. ! 
  echo Directory D:\Videos\example does not exist. You have deleted or moved the folder. 
  echo Rerun the rebuildCSV version of the wallpaperPlaylistLeftscreen__createBAT BAT file 
  echo Or just delete this file. 
  pause 
  exit 
) 
cd D:\Videos\example 
for /r %%b in (*.mp4) do "C:\Program Files (x86)\VideoLAN\VLC\vlc.exe" --no-audio & :: ARGUMENTS HERE "%%b" 
exit 
