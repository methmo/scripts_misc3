@echo off
set /p vidDirectory="Video directory: "
set inputFile="wallpaper_CSVVideoThemeMusic.txt"
set closePersonalisationWindow="closePersonalisationWindow.vbs"

copy /y "C:\BAT files\wallpaper_CSVVideoThemeMusic_backup3" "C:\BAT files\wallpaper_CSVVideoThemeMusic_backup4" & :: Cycles backups
copy /y "C:\BAT files\wallpaper_CSVVideoThemeMusic_backup2" "C:\BAT files\wallpaper_CSVVideoThemeMusic_backup3"
copy /y "C:\BAT files\wallpaper_CSVVideoThemeMusic_backup1" "C:\BAT files\wallpaper_CSVVideoThemeMusic_backup2"
copy /y "C:\BAT files\wallpaper_CSVVideoThemeMusic.txt" "C:\BAT files\wallpaper_CSVVideoThemeMusic_backup1"
break>"C:\BAT files\wallpaper_CSVVideoThemeMusic.txt" & :: Wipes the CSV database, so we can replace its contents nicely.

d: & :: This is because I stored my videos on my D: drive.
cd %vidDirectory%
for /r /d %%a in (*) do (
echo %%~na


:: The silent video wallpaper BAT. This is the version which is linked to a music playlist via the CSV.
echo @echo off > "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo set inputFile=^"wallpaper_CSVVideoThemeMusic.txt^" >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo setlocal EnableDelayedExpansion >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"

echo if not exist %%inputFile%% ^( >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo   echo. >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo   echo. >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo   echo ERROR! Or false positive. ^! >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo   echo CSV file %%inputFile%% does not exist. Rerun the rebuildCSV version of the wallpaperPlaylistLeftscreen__createBAT BAT file >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo   pause >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo   exit >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo ^) >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"

echo for /f ^"tokens=1-3 delims=,^" %%%%A in ^(%%inputFile%%^) do ^( >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo   if ^"%%%%~A^"==^"%%~na_silent^" ^( >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo     ^"C:\Users\Adam\AppData\Local\Microsoft\Windows\Themes\%%%%~B.theme^" >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo     timeout 2 >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"

echo     if not exist %closePersonalisationWindow% ^( >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo       echo. >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo       echo. >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo       echo ERROR! Or false positive! ^! >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo       echo %closePersonalisationWindow% does not exist or has been moved. This isnt actually a problem for the script. >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo       echo If you want to run the script and cannot find a copy of this file, just delete the lines of script that mention it. >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo       pause >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo       exit >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo     ^) >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"

echo     start ^"^" ^"%closePersonalisationWindow%^" >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"

echo 	 if not ^"%%%%~C^" == ^"^" ^( >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"

echo       if not exist ^"wmpPlaylist_%%%%~C.bat^" ^( >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo         echo. >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo         echo. >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo         echo ERROR! Or false positive! ^! >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo         echo ^"wmpPlaylist_%%%%~C.bat^" does not exist or has been moved or deleted. >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo         pause >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo         exit >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo       ^) >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"

echo       cmd /c ^"wmpPlaylist_%%%%~C.bat^" >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo     ^) >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo   ^) >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo ^) >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"

:: Creates a VBS script so that you can run this BAT without a CMD window visible.
echo CreateObject^(^"Wscript.Shell^"^).Run ^"^"^"C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat^"^"^",0 > "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent_hidden.vbs"
echo ErrorActionPreference = ^"Stop^" >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent_hidden.vbs"

echo taskkill /F /IM vlc.exe >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat" & :: Kills VLC, in case there is already a video background.
echo d: >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"

echo if not exist %%a ^( >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo   echo. >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo   echo. >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo   echo ERROR! Or false positive. ^! >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo   echo Directory %%a does not exist. You have deleted or moved the folder. >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo   echo Rerun the rebuildCSV version of the wallpaperPlaylistLeftscreen__createBAT BAT file >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo   echo Or just delete this file. >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo   pause >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo   exit >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo ^) >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"

echo cd %%a >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo for /r %%%%b in ^(*.mp4^) do "C:\Program Files (x86)\VideoLAN\VLC\vlc.exe" --no-audio & :: ARGUMENTS HERE ^"%%%%b^" >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_silent.bat"
echo exit >> "C:\BAT files\wallpaperPlaylistLeftScreen_%%~na_silent.bat"

echo %%~na_silent,%%~na_silent_THEME,>> "C:\BAT files\wallpaper_CSVVideoThemeMusic.txt"

echo %%~na_silent,%%~na_silent_THEME, written to "C:\BAT files\wallpaper_CSVVideoThemeMusic.txt"
echo.




:: The non-silent video wallpaper BAT. This isnt linked to a music playlist.
echo @echo off > "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo set inputFile=^"wallpaper_CSVVideoThemeMusic.txt^" >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo setlocal EnableDelayedExpansion >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"

echo if not exist %%inputFile%% ^( >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo   echo. >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo   echo. >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo   echo ERROR! Or false positive. ^! >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo   echo CSV file %%inputFile%% does not exist. Rerun the rebuildCSV version of the wallpaperPlaylistLeftscreen__createBAT BAT file >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo   pause >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo   exit >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo ^) >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"

echo for /f ^"tokens=1-3 delims=,^" %%%%A in ^(%%inputFile%%^) do ^( >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo   if ^"%%%%~A^"==^"%%~na^" ^( >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo     ^"C:\Users\Adam\AppData\Local\Microsoft\Windows\Themes\%%%%~B.theme^" >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo     timeout 2 >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"

echo     if not exist %closePersonalisationWindow% ^( >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo       echo. >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo       echo. >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo       echo ERROR! Or false positive! ^! >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo       echo %closePersonalisationWindow% does not exist or has been moved. This isnt actually a problem for the script. >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo       echo If you want to run the script and cannot find a copy of this file, just delete the lines of script that mention it. >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo       pause >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo       exit >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo     ^) >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"

echo     start ^"^" ^"%closePersonalisationWindow%^" >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"

echo 	 if not ^"%%%%~C^" == ^"^" ^( >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"

echo       if not exist ^"wmpPlaylist_%%%%~C.bat^" ^( >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo         echo. >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo         echo. >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo         echo ERROR! Or false positive! ^! >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo         echo ^"wmpPlaylist_%%%%~C.bat^" does not exist or has been moved or deleted. >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo         pause >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo         exit >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo       ^) >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"

echo       cmd /c ^"wmpPlaylist_%%%%~C.bat^" >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo     ^) >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo   ^) >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo ^) >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"

:: Creates a VBS script so that you can run this BAT without a CMD window visible.
echo CreateObject^(^"Wscript.Shell^"^).Run ^"^"^"C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat^"^"^",0 > "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_hidden.vbs"
echo ErrorActionPreference = ^"Stop^" >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na_hidden.vbs"

echo taskkill /F /IM vlc.exe >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo d: >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"

echo if not exist %%a ^( >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo   echo. >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo   echo. >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo   echo ERROR! Or false positive. ^! >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo   echo Directory %%a does not exist. You have deleted or moved the folder. >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo   echo Rerun the rebuildCSV version of the wallpaperPlaylistLeftscreen__createBAT BAT file >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo   echo Or just delete this file. >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo   pause >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo   exit >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo ^) >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"

echo cd %%a >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat"
echo echo This has probably been run successfully. >> "C:\BAT files\wallpaperPlaylistLeftScreen_%%~na.bat"
echo for /r %%%%b in ^(*.mp4^) do "C:\Program Files (x86)\VideoLAN\VLC\vlc.exe" & :: ARGUMENTS HERE ^"%%%%b^" >> "C:\BAT files\wallpaperPlaylistLeftscreen_%%~na.bat" & :: Note, you may require more or different arguments.
echo exit >> "C:\BAT files\wallpaperPlaylistLeftScreen_%%~na.bat"

echo %%~na,%%~na_THEME,>> "C:\BAT files\wallpaper_CSVVideoThemeMusic.txt"

)
echo Finished.
pause
