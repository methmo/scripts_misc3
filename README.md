# Simple Scripts
Simple bash scripts for routine, semi-routine, or easily automated tasks.

Uploading it here because my hard-drive died, and I'm waiting for a tiny screwdriver to arrive in order to attempt to recover some of the data.

These scripts are all written with Linux (specifically, Kubuntu) in mind, except of course for the batch scripts.

## Bash

### Distro Setup

#### Kubuntu 17.04
A modular Kubuntu setup which does all the hard work.

### Git
Some scripts that I (being very new to git) have found rather useful.

### Web Scraping
Small bash scripts that I have found useful for the automation of collecting data, or organising my web activity.

## Python

### Web Scraping
Old scripts that I found useful for one-off tasks.

### Turtle
Script to generate flags using basic geometric shapes in Turtle

## Batch
Ancient, inelegant batch scripts from when I primarily used Windows. It generated batch scripts to quickly play a music playlist, and a video wallpaper playlist (using VLC to emulate a video wallpaper). The batch scripts would then be called by a Rainmeter (https://www.github.com/rainmeter/rainmeter) script.
